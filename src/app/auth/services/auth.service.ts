import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { BASE_URL, USERS_URN } from 'src/app/config/api-routes.config';
import { TOKEN_LS_KEY } from './../../config/local-storage.config';
import { Auth } from './../interfaces/auth.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _auth: Auth | undefined;

  get auth() {
    return { ...this._auth };
  }

  constructor(private http: HttpClient) {}

  public isAuthenticated(): Observable<boolean> {
    if (!localStorage.getItem(TOKEN_LS_KEY)) {
      return of(false);
    }
    return this.verifyJwt(localStorage.getItem(TOKEN_LS_KEY)!);
  }

  public verifyJwt(jwt: string): Observable<boolean> {
    // simulación de verificar jwt
    return this.http.get<Auth>(`${BASE_URL}${USERS_URN}/${jwt}`).pipe(
      map((auth) => {
        this._auth = auth;
        return !!auth.id;
      })
    );
  }

  public login() {
    return this.http.get<Auth>(`${BASE_URL}${USERS_URN}/1`).pipe(
      tap((auth) => (this._auth = auth)),
      tap((auth) => localStorage.setItem(TOKEN_LS_KEY, auth.id))
    );
  }

  public logout() {
    this._auth = undefined;
    localStorage.removeItem(TOKEN_LS_KEY);
  }
}
