import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ConfirmComponent } from './../../components/confirm/confirm.component';
import { Hero, Publisher } from './../../interfaces/heroes.interface';
import { HeroesService } from './../../services/heroes.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styles: [
    `
      img {
        width: 100%;
        border-radius: 5px;
      }
    `,
  ],
})
export class AddComponent implements OnInit {
  publishers = [
    { id: 'DC Comics', desc: 'DC - Comics' },
    { id: 'Marvel Comics', desc: 'Marvel - Comics' },
  ];
  hero: Hero = {
    superhero: '',
    alterEgo: '',
    characters: '',
    firstAppearance: '',
    publisher: Publisher.DCComics,
    altImg: '',
  };

  get isSuperheroEmpty(): boolean {
    return !this.hero.superhero.trim().length;
  }

  get isAltImgEmpty(): boolean {
    return !this.hero.altImg || !this.hero.altImg.trim().length;
  }

  get isEditing(): boolean {
    return this.router.url.includes('edit');
  }

  constructor(
    private heroesService: HeroesService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackbar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  public ngOnInit(): void {
    if (!this.isEditing) return;
    this.activatedRoute.params
      .pipe(switchMap(({ id }) => this.heroesService.getById(id)))
      .subscribe((hero) => (this.hero = hero));
  }

  public save() {
    if (this.isSuperheroEmpty || this.isAltImgEmpty) {
      return alert('Complete los campos requeridos');
    }
    if (this.isEditing) {
      this.heroesService.update(this.hero).subscribe((hero) => {
        this.showSnackbar('Hero actualizado exitosamente');
      });
    } else {
      this.heroesService.create(this.hero).subscribe((hero) => {
        this.showSnackbar('Hero creado exitosamente');
        this.router.navigate(['/heroes/edit', hero.id]);
      });
    }
  }

  public delete() {
    const dialog = this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: { ...this.hero }, // evitar cambios por referencia
    });

    dialog.afterClosed().subscribe((result) => {
      if (!result) return;
      this.heroesService.delete(this.hero.id!).subscribe((res) => {
        this.showSnackbar('Hero eliminado exitosamente');
        this.backToHeroes();
      });
    });
  }

  public showSnackbar(message: string) {
    this.snackbar.open(message, 'OK', { duration: 2500 });
  }

  public backToHeroes() {
    this.router.navigate(['/heroes/list']);
  }
}
