import { Component } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Hero } from './../../interfaces/heroes.interface';
import { HeroesService } from './../../services/heroes.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [],
})
export class SearchComponent {
  query: string = '';
  heroes: Hero[] = [];
  selectedHero: Hero | undefined;

  constructor(private heroesService: HeroesService) {}

  search() {
    this.heroesService
      .findByQuery(this.query.trim())
      .subscribe((heroes) => (this.heroes = heroes));
  }

  selectOption(event: MatAutocompleteSelectedEvent) {
    if (!event.option.value) {
      this.selectedHero = undefined;
      return;
    }
    const hero: Hero = event.option.value;
    this.query = hero.superhero;

    this.heroesService
      .getById(hero.id!)
      .subscribe((hero) => (this.selectedHero = hero));
  }
}
