import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  BASE_URL,
  HEROES_URN,
  LIMIT_PARAM_KEY,
  QUERY_PARAM_KEY,
} from 'src/app/config/api-routes.config';
import { Hero } from './../interfaces/heroes.interface';

@Injectable({
  providedIn: 'root',
})
export class HeroesService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<Hero[]> {
    return this.http.get<Hero[]>(`${BASE_URL}${HEROES_URN}`);
  }

  getById(heroId: string): Observable<Hero> {
    return this.http.get<Hero>(`${BASE_URL}${HEROES_URN}/${heroId}`);
  }

  findByQuery(query: string): Observable<Hero[]> {
    const params = new HttpParams()
      .set(QUERY_PARAM_KEY, query)
      .set(LIMIT_PARAM_KEY, 6);
    return this.http.get<Hero[]>(`${BASE_URL}${HEROES_URN}`, {
      params,
    });
  }

  create(hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(`${BASE_URL}${HEROES_URN}`, hero);
  }

  update(hero: Hero): Observable<Hero> {
    return this.http.put<Hero>(`${BASE_URL}${HEROES_URN}/${hero.id}`, hero);
  }

  delete(heroId: string): Observable<void> {
    return this.http.delete<void>(`${BASE_URL}${HEROES_URN}/${heroId}`);
  }
}
