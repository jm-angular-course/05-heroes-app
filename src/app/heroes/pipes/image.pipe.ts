import { Pipe, PipeTransform } from '@angular/core';
import { Hero } from './../interfaces/heroes.interface';

@Pipe({
  name: 'image',
  // pure: false, //se vuelve a invocar cada ciclo de detección de cambios
})
export class ImagePipe implements PipeTransform {
  transform(hero: Hero): string {
    if (!hero.id) {
      return 'assets/no-image.png';
    }
    if (hero.altImg) {
      return hero.altImg;
    }
    return `assets/heroes/${hero.id}.jpg`;
  }
}
