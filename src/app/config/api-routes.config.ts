import { environment } from '../../environments/environment';

export const BASE_URL: string = environment.baseUrl;
export const USERS_URN: string = '/usuarios';
export const HEROES_URN: string = '/heroes';
export const QUERY_PARAM_KEY: string = 'q';
export const LIMIT_PARAM_KEY: string = '_limit';
